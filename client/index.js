import './assets/styles/global.styl'
import $ from 'jquery';

// banner 已废弃


// 导航更多hover
$('.nav-more').hover(function () {
  $(this).find('.nav-more-layer').show(200)
}, function () {
  $(this).find('.nav-more-layer').hide(100)
})

//获取导航条距离顶部位置
var x = $('.nav_list').offset().top;
//获取免责声明位置
var shengming = $('#shengming').offset().top;
$('#sidebar').css('top', '-'+shengming + 'px')
//滚动执行
window.onscroll = function () {
	//获取当前滚动条位置
	var doc = $(document).scrollTop();
	//顶部固定
	if (doc > x) {
		$('.nav_list').addClass('Selected');
	} else {
		$('.nav_list').removeClass('Selected')
	}
	//免责声明顶部固定
	if (doc > shengming) {
		$('#sidebar').addClass('action')
		$('#sidebar').css('top', '0px')
	} else {
		$('#sidebar').css('top', '-'+shengming + 'px')
		$('#sidebar').removeClass('action')
  }
  
  // 回到顶部
  if (doc > 600) {
    $("#gotop").fadeIn('fast');
  } else {
    $("#gotop").fadeOut('fast');
  }
}

// swiper PC Banner
var banner =  new Swiper('.scrollContainer .swiper-container', {
  // effect : 'fade',
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  autoplay: {
    delay: 3000,
    stopOnLastSlide: false,
    disableOnInteraction: false,
  },
  loop : true,
});





// 回到顶部
$("#gotop").click(function () {
  $("html,body").animate({ scrollTop: 0 }, 200);
  return false;
});
