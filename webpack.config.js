const path = require('path')
const HTMLPlugin = require('html-webpack-plugin')
const webpack = require('webpack')
const ExtractPlugin = require('extract-text-webpack-plugin')


const isDev = process.env.NODE_ENV === 'development'

const config = {
	externals: {
		jquery: 'jQuery'
	},
	target: 'web', // 指定是web平台
	entry: path.join(__dirname, './client/index.js'),	// 入口文件
	output: {
		filename: 'bundle.[hash:8].js',
		path: path.join(__dirname, 'dist')
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: 'babel-loader'
			},
			{
				test: /\.(gif|jpg|jpeg|png|svg)$/,
				use: [
					{
						loader: 'url-loader', // url-loader 用于吧图片一直转换层base64
						options: {
							limit: 3000,
							name: '[name]-aaa.[ext]'
						}
					}
				]
			}
		]
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: isDev ? '"development"' : '"production"'
			}
		}),
		new HTMLPlugin({
			template: path.resolve(__dirname, './client/index.html')
		})
	]
}


// 根据环境设置
if (isDev) {
	config.module.rules.push({
		test: /\.styl$/,
		use: [
			 
			'style-loader',
			'css-loader',
			{
				loader: 'postcss-loader',
				options: {
					sourceMap: true,
				}
			},
			'stylus-loader'
		]
	}, )
	config.devtool = '#cheap-module-eval-source-map'
	config.devServer = {
		port: 8080,
		host: '0.0.0.0',
		overlay: {
			errors: true,  // 编译过程中发生错误直接显示在网页上
		},
		// hot: true, // 无刷新
		// historyFallback: {}
		// open: true, // 启动webpack时打开默认浏览器
		contentBase: '/', // 托管目录. 在publicPath未设置的情况下, webpack的bundle结果会创建在devServer域名根目录
		// historyApiFallback: false, // h5的history模式，可以任何链接都返回index.html
		inline: true, // 实时刷新 (全量刷新)
		proxy: {
			'/': 'http://localhost:3000'
		}
	}
	config.plugins.push(
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoEmitOnErrorsPlugin(),
		// new ExtractPlugin()
	)
} else {
	config.entry = {
		app: path.join(__dirname, './client/index.js'),	// 入口文件
		// header: path.join(__dirname, './client/header.js'),	// 入口文件
	}
	config.output.filename = '[name].[chunkhash:8].js'
	config.module.rules.push(
		{
			test: /\.styl$/,
			use: ExtractPlugin.extract({
				fallback: 'style-loader',
				use: [
					{
						loader: 'css-loader',
						options: {
							minimize: true //css压缩
						}
					},
					{
						loader: 'postcss-loader',
						options: {
							sourceMap: true,
						}
					},
					'stylus-loader'
				]
			})
		}
	)
	config.plugins.push(
		new ExtractPlugin('styles.[contentHash:8].css'),
		new webpack.optimize.UglifyJsPlugin({
			compress: {
				warnings: false
			},
			output: {
				// 去掉JS注释内容
				comments: false,
			},
		}),
	)
}

module.exports = config